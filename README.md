# ArchLabs Installer

#### Features

- LUKS/LVM setup
- Package & Session selection
- Full device auto partition

#### Requirements

`awk`, `sed`, `curl`, `vim`, `rsync`, `parted`, `dialog`, `coreutils`, `findutils`, `chpasswd`, `util-linux`, `arch-install-scripts`


#### Manual Installation

```
curl -fsSL https://bitbucket.org/archlabslinux/installer/raw/master/archlabs-installer -o /usr/bin/archlabs-installer
```

---

A packaged version can also be found in our repos:

- stable: https://bitbucket.org/archlabslinux/archlabs\_repo

- unstable: https://bitbucket.org/archlabslinux/archlabs\_unstable

